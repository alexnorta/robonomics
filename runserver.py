# import the create app application factory
from src import create_app


app = create_app()

print("app", __name__)
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
