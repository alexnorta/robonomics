# Set up tool
- Set python flask framework
- Run Robonomics node
- Run ipfs daemon

# Tools
- Python 3 - The language programming used
- Flask - The web framework used
- Virtualenv - The virtual environment used
- SQL Alchemy - The database library
- Flask-SQLAlchemy - Flask and SQL Alchemy connector
- IPFS - IPFS for file storage

# Functional Requirement

1 : Create a post endpoint to get data from dapp web
2: Send the data to ipfs
3: Get the hash of the data from ipfs
4: We send the hash to Robonomics

Item table
- The Token Identifier (or ID) ie unique ide for that token
- The Owner of the Token ie the address of the owner
- The Metadata associated with the token ie a column that may contain data of the NFT or about the NFT.

This JSON file needs to have information about the NFT such as its name, description, image URL, attributes, etc

in order to make sure that everyone in the ecosystem (including NFT marketplaces like OpenSea) understands what’s in our JSON files, we need to format them in a way that is compliant with the standards.
my-nft.json

{   
   "description": "Friendly OpenSea Creature",      
   "image": "https://opensea-prod.appspot.com/puffs/3.png",  
   "name": "Dave Starbelly",   
   "attributes": [
       { "trait_type": "Base", "value": "Starfish" },      
       { "trait_type": "Eyes", "value": "Big" },      
       { "trait_type": "Mouth","value": "Surprised" },
   ]
}

Storing metadata in this format on the blockchain is still very expensive. Hence, we add an additional layer of abstraction, and upload this JSON to the cloud as well and simply store a URL pointing to the JSON file.

Therefore, at the end of the data, all you’re storing on the blockchain is https://mywebsite.com/my-nft.json.

# To summarize, here is what we need to do:

- Upload all our images online and get a URL associated with each image. (This URL will go into our metadata).
- Generate a separate JSON file for each image containing metadata in the standard shown above (Image URL, attributes/traits, name, etc.)
- Upload all the JSON files to the cloud and get a URL associated with each JSON file.

1: Uploading Images to IPFS
2: With the CID, Generate compliant NFT JSON metadata
Our next task is to create a JSON file for each image and populate it with data (including the image URL) in a format that is compliant and understandable by platforms like NFT marketplaces
3: Upload JSON metadata files to IPFS
The third step is probably the simplest. Just like you did with the images, upload your json folder to Pinata.

 # download pre-compiled IPFS:

sudo snap install ipfs

# Compile the codebase in Go
Clone the repository and run the install script in the Makefile.

wget https://dist.ipfs.io/go-ipfs/v0.4.15/go-ipfs_v0.4.15_linux-amd64.tar.gz
tar xvfz go-ipfs_v0.4.15_linux-amd64.tar.gz
rm go-ipfs_v0.4.15_linux-amd64.tar.gz 
sudo mv go-ipfs/ipfs /usr/local/bin
rm -rf go-ipfs

# Next step
echo 'export IPFS_PATH=/data/ipfs' >>~/.bash_profile
source ~/.bash_profile
sudo mkdir -p $IPFS_PATH
sudo chown ubuntu:ubuntu $IPFS_PATH
ipfs init -p server

# Setup ipfs to run everytime
sudo bash -c 'cat >/lib/systemd/system/ipfs.service <<EOL
[Unit]
Description=ipfs daemon
[Service]
ExecStart=/usr/local/bin/ipfs daemon --enable-gc
Restart=always
User=ubuntu
Group=ubuntu
Environment="IPFS_PATH=/data/ipfs"
[Install]
WantedBy=multi-user.target
EOL'

# And then enable our new service,

```sudo systemctl daemon-reload```
```sudo systemctl enable ipfs.service```

# And start it, and check it out (a sanity check if you will):

```sudo systemctl start ipfs```
```sudo systemctl status ipfs```


# Managed to do this by running the following commands:

ipfs config Addresses.API /ip4/0.0.0.0/tcp/5001
ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/8080


image -> ipfs > hash

metadata = {
    image: hash,
    description: "antying,
    name: "laptop
}

ipfs = hash

1: Spin Robonomics node on aws
2: Spin the python api with docker and deploy on aws
3: Deploying ipfs 

# robonomics.service
```
[Unit]
Description=Robonomics node
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/usr/bin/robonomics --dev
User=feecc000001
Restart=always

[Install]
WantedBy=multi-user.target

```
 # Robonomics Interface
 https://multi-agent-io.github.io/robonomics-interface/usage.html

 # Environment variables
- MONGODB_URI - Your MongoDB connection URI ending with /db-name.

- ROBONOMICS_ENABLE_DATALOG- Whether to post CIDs to Robonomics network datalog or not. Defaults to false.
- ROBONOMICS_SUBSTRATE_NODE_URL - Robonomics node URL in case you want to use a non-default node.
- ROBONOMICS_ACCOUNT_SEED- Robonomics network account seed. Leave empty if you don't need it.
- AUTH_MODE- Authentication mode. Available options are "analytics" (auth by analytics login), "workbench" (auth by card id) and "noauth" (auth w/o credentials).


Steps to access the linked metadata:
Step 1: Create separate directories for assets and metadata.

Step 2: Add assets to newly created directories.

Step 3: Add the asset catalog to IPFS and note its CID.

Step 4: Generate metadata in your directory with CID reference assets for creating IPFS URIs. Make sure that the URI consists of the file name of CID and asset of the directory.

Step 5: Now, keep adding metadata directory to IPFS and note its CID.

Step 6: Generate an IPFS URI with the noted CID and store it to create an ownership record on the chain.