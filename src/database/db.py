from flask_sqlalchemy import SQLAlchemy  # pylint: disable=import-error

# Initialize SQLAlchemy with no settings
db = SQLAlchemy()

def init_app(self, app):
        super(SQLAlchemy, self).init_app(app)

        # database_uri = app.config['SQLALCHEMY_DATABASE_URI']
        # assert database_uri, "SQLALCHEMY_DATABASE_URI must be configured!"
        # if database_uri.startswith('sqlite:'):
        #     self.event.listens_for(engine.Engine, "connect")(set_sqlite_pragma)

        # app.extensions['migrate'] = AlembicDatabaseMigrationConfig(self, compare_type=True)