from flask_sqlalchemy import SQLAlchemy  # pylint: disable=import-error
from flask import Flask, jsonify

from src.constants.http_status_codes import HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
from .database.db import db
from .config.config import DevelopmentConfig
# Import extensions
from .extensions.extensions import cors, mail
from .routes.ipfs_route import ipfs
from .routes.brand_route import brand_record
from .routes.transaction_route import transaction_record
from .routes.account_route import account_record
from .utils.logger import get_logger

logger = get_logger("logisticBDT")


def create_app(config=DevelopmentConfig):
    """Creates a new Flask application and initialize application."""
    logger.info("Creating Flask Application Factory")
    app = Flask(__name__)
    app.config.from_object(config)

    # app.config['SQLALCHEMY_DATABASE_URI'] = DevelopmentConfig.SQLALCHEMY_DATABASE_URI

    """SQLAlchemy configuration"""
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}'.format(
        USER=DevelopmentConfig.DB_USERNAME,
        PASSWORD=DevelopmentConfig.DB_PASSWORD,
        HOST=DevelopmentConfig.DB_HOST,
        PORT=DevelopmentConfig.DB_PORT,
        DATABASE=DevelopmentConfig.DB_NAME
    )
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = DevelopmentConfig.SQLALCHEMY_TRACK_MODIFICATIONS

    app.config["MAIL_SERVER"] =DevelopmentConfig.MAIL_SERVER
    app.config["MAIL_PORT"] = DevelopmentConfig.MAIL_PORT
    app.config["MAIL_USE_TLS"] = DevelopmentConfig.MAIL_USE_TLS
    app.config["MAIL_USERNAME"] = DevelopmentConfig.MAIL_USERNAME
    app.config["MAIL_PASSWORD"] = DevelopmentConfig.MAIL_PASSWORD
    app.config['MAIL_USE_SSL'] = DevelopmentConfig.MAIL_USE_SSL

    db.app = app
    db.init_app(app)
    logger.info("Initializing db")
    print("db", db)
    logger.info("Initialized the database.")
    
    @app.before_first_request
    def create_tables():
        db.create_all()
    # Load route endpoints
    @app.route('/', methods=['GET'])
    def index():
        """
        example endpoint
        """
        return 'Welcome to LogisticBDT API'

    _register_extensions(app)
    _register_blueprints(app)
    _initialize_errorhandlers(app)
   

    return app


def _register_extensions(app: Flask):
    """Registers extensions with app config"""
    cors.init_app(app)
    mail.init_app(app)

    pass

def _initialize_errorhandlers(app):
    '''
    Initialize error handlers
    '''
    @app.errorhandler(HTTP_404_NOT_FOUND)
    def page_not_found(e):
        return jsonify({'error': 'The resource could not be found'}), HTTP_404_NOT_FOUND

    @app.errorhandler(HTTP_500_INTERNAL_SERVER_ERROR)
    def handle_500(error):
        message = [str(x) for x in error.args]
        success = False
        response = {
            'success': success,
             "code": error.code,
             "name": error.name,
             "description": error.description,
             'error': {
                'type': error.__class__.__name__,
                'message': message
            }
        }

        return jsonify(response), HTTP_500_INTERNAL_SERVER_ERROR
    
def _register_blueprints(app: Flask):
    """Registers blueprints"""
    app.register_blueprint(ipfs)
    app.register_blueprint(brand_record)
    app.register_blueprint(transaction_record)
    app.register_blueprint(account_record)