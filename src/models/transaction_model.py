from dataclasses import dataclass
from datetime import datetime
from src.database.db import db  # pylint: disable=import-error
from sqlalchemy.exc import IntegrityError, SQLAlchemyError  # pylint: disable=import-error


@dataclass
class Transaction(db.Model):
    """Transaction table properties
    Fields:
        brand_public_key: brand_public_key of a transaction
        pid: pid of a transaction
        cid: cid of a transaction
        pk1: pk1 of a transaction
        pk2: pk2 of a transaction
        rb_index: rb_index of a transaction

    """
    id = db.Column('transaction_id', db.Integer,
                   primary_key=True, autoincrement=True)
    brand_public_key = db.Column(db.String(100), nullable=False)
    pid = db.Column(db.String(100), nullable=False)
    cid = db.Column(db.String(100), nullable=False)
    pk1 = db.Column(db.String(100), nullable=False)
    pk2 = db.Column(db.String(100), nullable=False)
    rb_index = db.Column(db.String(100), nullable=False)
    datalog_index = db.Column(db.String(20), nullable=False)
    last_transaction = db.Column(db.Boolean(), nullable=False,  default=False)

    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __init__(self, brand_public_key, pid, cid, pk1, pk2, rb_index, datalog_index, last_transaction):
        self.brand_public_key = brand_public_key
        self.pid = pid
        self.cid = cid
        self.pk1 = pk1
        self.pk2 = pk2
        self.rb_index = rb_index
        self.datalog_index = datalog_index
        self.last_transaction = last_transaction

        """This is used for debugging purposes
        """

    def __repr__(self):
        return f"{self.brand_public_key}:{self.pid}:{self.cid}:{self.pk1}:{self.pk2}:{self.rb_index}:{self.datalog_index}"

    @staticmethod
    def register_a_transaction(json_result, ipfs_results, robonomics_hash, brand, datalog_index):
        """
        Create a new transaction.

        :type data: data

        :return: Optional[Transaction]
        """
        try:
            brand_public_key = brand.public_key
            pid = json_result['pid']
            cid = ipfs_results['cid']
            pk1 = json_result['public_key']
            pk2 = json_result['public_key']
            rb_index = robonomics_hash
            new_transaction = Transaction(brand_public_key=brand_public_key, pid=pid,
                                          cid=cid, pk1=pk1, pk2=pk2, rb_index=rb_index, datalog_index=datalog_index, last_transaction=True)
            db.session.add(new_transaction)
            db.session.commit()
            return new_transaction
        except IntegrityError as error:
            db.session.rollback()
            return {"error": error.orig}
        except SQLAlchemyError as error:
            return {"error": error}

    @staticmethod
    def transfer_ownership(json_result, ipfs_results, robonomics_hash, found_pid, buyer_public_key, datalog_index):
        """
            Create a new transaction for new owner.

            :type data: data

            :return: Optional[Transaction]
            """
        try:
            brand_public_key = found_pid.brand_public_key
            pid = json_result['pid']
            cid = ipfs_results['cid']
            pk1 = found_pid.pk2
            pk2 = buyer_public_key
            rb_index = robonomics_hash
            datalog_index=datalog_index
            new_transaction = Transaction(brand_public_key=brand_public_key, pid=pid,
                                          cid=cid, pk1=pk1, pk2=pk2, rb_index=rb_index, datalog_index=datalog_index, last_transaction=True)
            db.session.add(new_transaction)
            db.session.commit()
            return new_transaction
        except IntegrityError as error:
            db.session.rollback()
            return {"error": error.orig}
        except SQLAlchemyError as error:
            return {"error": error}

    @staticmethod
    def update_an_item_by_owner_last_transaction(pk2, pid):
        """
            Get current owner of an item

            :type data: pk2
            :type data: pid

            :return: Optional[Transaction]
            """
        try:
            found_items = Transaction.query.filter_by(pk2=pk2, pid=pid, last_transaction = True).first()
            if found_items:
                found_items.last_transaction = False
                db.session.commit()
        except IntegrityError as error:
            db.session.rollback()
            return {"error": error.orig}
        except SQLAlchemyError as error:
            return {"error": error}
