import re

from dataclasses import dataclass
from datetime import datetime
from sqlalchemy.orm import validates  # pylint: disable=import-error
from src.database.db import db  # pylint: disable=import-error


@dataclass
class Brand(db.Model):
    """Brand table properties
    Fields:
        name: name of a brand
        public_key: public key that belongs to a brand that owns a luxury item
        host_address: host address that belongs to a brand that owns a luxury item
        host_email: host email that belongs to a brand that owns a luxury item
        status: status that belongs to a brand that owns a luxury item
    """
    id = db.Column('brand_id', db.Integer,
                   primary_key=True, autoincrement=True)
    brand_name = db.Column(db.String(20), nullable=False)
    public_key = db.Column(db.String(100), nullable=False)
    host_address = db.Column(db.String(50), nullable=False, unique=True)
    host_email = db.Column(db.String(50), nullable=False, unique=True)
    secrets_id = db.Column(db.String(100), nullable=True)
    status = db.Column(db.Boolean(), nullable=False,  default=False)
    verified = db.Column(db.Boolean(), nullable=False, default=False)

    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __init__(self, brand_name, public_key, host_address, host_email, status):
        self.brand_name = brand_name
        self.public_key = public_key
        self.host_address = host_address
        self.host_email = host_email
        self.status = status

        """This is used for debugging purposes
        """
    @validates("host_email")
    def validate_email(self, key, host_email):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if not host_email:
            raise AssertionError('No email provided')
        if not re.fullmatch(regex, host_email):
            raise AssertionError('Provided email is not an email address')
        return host_email

    def __repr__(self):
        return f"{self.brand_name}:{self.public_key}:{self.host_address}:{self.host_email}"

    @staticmethod
    def get_brand_by_public_key(public_key):

        brand = Brand.query.filter_by(public_key=str(public_key)).first()
        if brand:
            return brand
        return "No brand found"

    @staticmethod
    def update_brand_with_secret_id(brand_id, token_id):
        print("Update brand with secret", brand_id, token_id)
        brand = Brand.query.filter_by(id=brand_id).first()
        if brand:
            brand.secrets_id = token_id
            db.session.commit()