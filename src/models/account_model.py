from dataclasses import dataclass
from datetime import datetime
from src.database.db import db  # pylint: disable=import-error
from werkzeug.security import generate_password_hash, check_password_hash


@dataclass
class Account(db.Model):
    """Account table properties
    Fields:
        wallet_name: wallet name of a Account
        keystone_json_file: Account keystone json file
    """
    id = db.Column('Account_id', db.Integer,
                   primary_key=True, autoincrement=True)
    wallet_name = db.Column(db.String(80), nullable=False, unique=True)
    keystone_json_file = db.Column(db.String(20000), nullable=False)
    password = db.Column(db.String(255), nullable=False, unique=True)
    address = db.Column(db.String(255), nullable=False, unique=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __init__(self, wallet_name, keystone_json_file, password, address):
        self.wallet_name = wallet_name
        self.keystone_json_file = keystone_json_file
        self.password = generate_password_hash(password)
        self.address = address

        """This is used for debugging purposes
        """
    def __repr__(self):
        return f"{self.wallet_name}:{self.keystone_json_file}"
    
    def verify_password(self, password):
        print("Verifying password: {}".format(password))       
        print("in password: {}".format(self.password))      
        return check_password_hash(self.password, password)

        # verify endpoint = this checks whether address already exist, return true or false
        # confirm login, with password and address, return true or false
