"""ENV: Flask application environment.
Examples: `development`, `production`, `testing`.
"""
import os

# set the base directory
basedir = os.path.abspath(os.path.dirname(__name__))

# Create the super class

print('basedir', basedir)


class BaseConfig(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    HOST = "0.0.0.0"
    PORT = 5000
    DEBUG = False

# Create the development config


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    IPFS_CONNECT_URL = os.environ.get('IPFS_CONNECT_URL')
    IPFS_PORT = os.environ.get('IPFS_PORT')
    DOMAIN = os.environ.get("DOMAIN")

    #    # Connect to the database
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    # # Turn off the Flask-SQLAlchemy event system and warning
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get(
        'SQLALCHEMY_TRACK_MODIFICATIONS')
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    DB_HOST= os.environ.get("DB_HOST")
    DB_PORT=os.environ.get("DB_PORT")
    DB_USERNAME=os.environ.get("DB_USERNAME")
    DB_PASSWORD=os.environ.get("DB_PASSWORD")
    DB_NAME=os.environ.get("DB_NAME")

    # AES CRYPTOGRAPHY
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SECRET_IV = os.environ.get('SECRET_IV')

 # ROBONOMICS
    ROBONOMICS_ACCOUNT = os.environ.get('ROBONOMICS_ACCOUNT_SEED')

 # Email config
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = os.environ.get('MAIL_PORT')
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') 
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', True)
    MAIL_USE_SSL = os.environ.get('MAIL_USE_TLS', False)
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')

# Create the testing config


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    IPFS_CONNECT_URL = os.environ.get('IPFS_CONNECT_URL')


# create the production config
class ProductionConfig(BaseConfig):
    DEBUG = False
    IPFS_CONNECT_URL = os.environ.get('IPFS_CONNECT_URL')
    IPFS_PORT = os.environ.get('IPFS_PORT')
    #Connect to the database
   #  SQLALCHEMY_DATABASE_URI = 'sqlite:///' + \
   #      os.path.join(basedir, 'database.db')
    # # Turn off the Flask-SQLAlchemy event system and warning
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
       

 # ROBONOMICS
    ROBONOMICS_ACCOUNT = os.environ.get('ROBONOMICS_ACCOUNT_SEED')

 # Email config
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = os.environ.get('MAIL_PORT')
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') 
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', True)
    MAIL_USE_SSL = os.environ.get('MAIL_USE_TLS', False)
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')
