import ipfsapi
import typing as tp

from src.utils.logger import get_logger  # pylint: disable=import-error
from src.config.config import DevelopmentConfig  # pylint: disable=import-error
from datetime import datetime

logger = get_logger(__name__)
METADATA_TEMPLATE = {
    "product_name": "",
    "description": "",
    "product_image": "",
    "attributes": [],
    "timestamp": ""
}

IPFS_RESULT_TEMPLATE = {
    "url": "",
    "cid": "",
}


class IpfsHttpClient:
    """
     Class for interacting with IPFS 

    Relay IPFS is a IPFS node that spreads luxury items. Since IPFS node in the browser doesn't spread the file
    itself, this helps the clients(artists) uploaded files to be spread out more rapidly.
    """

    def __init__(self):
       # Connect to local node
        try:
            self._client = ipfsapi.connect(
                DevelopmentConfig.IPFS_CONNECT_URL, DevelopmentConfig.IPFS_PORT)
            logger.info("Successfully connected to the IPFS node")
        except ConnectionError as error:
            logger.error(
                f"An error occurred while getting IPFS client: {error}")

    def get_connection(self):
        return self._client

    def get_single_item(self, cid: tp.Optional[str] = None) -> str:
        """Loads a json object from IPFS.

        Parameters
                ----------
                cid : Union[str, cid.CIDv0, cid.CIDv1]
                   CID of the IPFS object to load
                Returns
                -------
                        object
                                Deserialized IPFS JSON object value
        """
        res = self._client.get_json(cid)
        return res

    def upload_json_metadata(self, input_data, brand, message):
        """
        Adds a json-serializable Python dict as a json file to IPFS
        :parameter input_path: Path to the input file
        :type input_data: Path
        :return tuple consisting of IPFS hash and gateway link to it
        """
        token_metadata = METADATA_TEMPLATE
        token_metadata["product_name"] = input_data['product_name']
        token_metadata["description"] = input_data['description']
        token_metadata["product_image"] = input_data['product_image']
        token_metadata["timestamp"] = str(datetime.now())
        token_metadata["attributes"] = [
            {"trait_type": "Product brand",
                "value": input_data['product_brand']},
            {"trait_type": "Product label",
                "value": input_data['product_label']},
            {"trait_type": "Color", "value": input_data['product_color']},
            {"trait_type": "Product Serial No", "value": input_data['pid']},
            {"trait_type": "PK1", "value": input_data['public_key']},
            {"trait_type": "PK2", "value": input_data['public_key']},
            {"trait_type": "Signed_data", "value": message},
            {"trait_type": "Minted By", "value": brand.host_address},

        ]
        print("serializable timestamp", token_metadata)
        try:
            res = self._client.add_json(token_metadata)
            logger.info("hash from ipfs: {res}")
            print("url", DevelopmentConfig.IPFS_CONNECT_URL)
            url = "https://" + DevelopmentConfig.IPFS_CONNECT_URL + '/ipfs/' + res
            print("url ipfs", url)
            result_from_ipfs = IPFS_RESULT_TEMPLATE
            result_from_ipfs["url"] = url
            result_from_ipfs["cid"] = res
            return result_from_ipfs
            # return res['Hash']
        except Exception as error:
            logger.error('Upload Error! exception caused by %s', error)
            return "Upload Error! \n", 503

    def upload_json_metadata_on_transfer(self, input_data, found_pid, new_pk_owner, message):
        """
        Adds a json-serializable Python dict as a json file to IPFS
        :parameter input_path: Path to the input file
        :type input_data: Path
        :return tuple consisting of IPFS hash and gateway link to it
        """
        token_metadata = METADATA_TEMPLATE
        token_metadata["product_name"] = input_data['product_name']
        token_metadata["description"] = input_data['description']
        token_metadata["product_image"] = input_data['product_image']
        token_metadata["timestamp"] = str(datetime.now())
        token_metadata["attributes"] = [
            {"trait_type": "Product brand",
                "value": input_data['product_brand']},
            {"trait_type": "Product label",
                "value": input_data['product_label']},
            {"trait_type": "Color", "value": input_data['product_color']},
            {"trait_type": "Product Serial No", "value": input_data['pid']},
             {"trait_type": "PK1", "value": found_pid.pk1},
            {"trait_type": "PK2", "value": new_pk_owner},
            {"trait_type": "Signed_data", "value": message},
            {"trait_type": "Minted By", "value": input_data["minted_by"]},

        ]
        print("serializable timestamp", token_metadata)
        try:
            res = self._client.add_json(token_metadata)
            logger.info("hash from ipfs: {res}")
            print("url", DevelopmentConfig.IPFS_CONNECT_URL)
            url = "https://" + DevelopmentConfig.IPFS_CONNECT_URL + '/ipfs/' + res
            print("url ipfs", url)
            result_from_ipfs = IPFS_RESULT_TEMPLATE
            result_from_ipfs["url"] = url
            result_from_ipfs["cid"] = res
            return result_from_ipfs
            # return res['Hash']
        except Exception as error:
            logger.error('Upload Error! exception caused by %s', error)
            return "Upload Error! \n", 503

    def add_bytes(self, raw_bytes):
        res = self._client.add(raw_bytes)
        url = "https://" + DevelopmentConfig.IPFS_CONNECT_URL + \
            '/ipfs/' + res["Hash"]
        return url

    def add(self, raw_str):
        return self._client.add_str(raw_str)

    def get_bytes(self, cid):
        return self._client.cat(cid)

    def get(self, cid):
        return self.get_bytes(cid).decode('utf-8')

    def ls_pins(self):
        response = self._client.pin.ls()
        return list(response['Keys'].keys())

    def add_pin(self, cid):
        self._client.pin.add(cid)

    def delete_file(self, file_hash):
        """_Unpin and remove file from local node. Warning! This method collects garbage: all unpinned items will be removed

        Args:
            param client: ipfshttpclient ot interact with IPFS node
             :param file_hash: IPFS file hash

        :return success flag
        """
        try:
            self._client.pin.rm(file_hash)
        except self._client.exceptions.ErrorResponse:
            return False

        return True

    def update_pin(self, old_cid, new_cid):
        self._client.pin.update(old_cid, new_cid)

    def ipfs_add(self, item, is_file=True):
        """This function is used to add item to ipfs

        Args:
            item (_type_): _description_
            isFile (bool, optional): _description_. Defaults to True.

        Returns:
            _type_: _description_
        """
        if (is_file):
            res = self._client.add(item)
        elif (isinstance(item, str)):
            res = self._client.add_str(item)
        elif (isinstance(item, dict)):
            res = self._client.add_json(item)
        elif (isinstance(item, bytes)):
            res = self._client.add_bytes(item)
        else:
            try:
                res = self._client.add_pyobj(item)
            except Exception as e:
                res = None

        return res

    def ipfs_get_item(self, itemhash):
        return self._client.get_json(itemhash)
