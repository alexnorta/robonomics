import typing as tp

from substrateinterface import Keypair, KeypairType  # pylint: disable=import-error

# Verify generated signature with public address


def verify_signature(message: tp.Optional[str] = None, signature: tp.Optional[str] = None, address: tp.Optional[str] = None) -> bool:
    """This is used to verify message sign by Polkadot

    Args:
        message (String): message to be verified in `Scalebytes`, bytes or hex string format
        signature (String): signature in bytes or hex string format
        address (String): _address of the user that sign the message_

    Returns:
        Boolean: True if data is signed with this Keypair, otherwise False
    """
    keypair_public = Keypair(ss58_address=address,
                             crypto_type=KeypairType.SR25519)
    is_valid = keypair_public.verify(message, signature)

    return is_valid
