import typing as tp

from robonomicsinterface import Account, Datalog  # pylint: disable=import-error

from src.classes.datalog_response import DataLogResponse
from src.utils.logger import get_logger
from src.config.config import DevelopmentConfig  # pylint: disable=import-error

logger = get_logger(__name__)

ROBONOMICS_ACCOUNT: tp.Optional[Account] = None
DATALOG_CLIENT: tp.Optional[Datalog] = None
DatalogTyping = tp.Tuple[int, tp.Union[int, str]]

ROBONOMICS_ACCOUNT = Account(
    seed=DevelopmentConfig.ROBONOMICS_ACCOUNT, remote_ws="wss://logistic-bdt.tk")


class RobonomicsClient:
    """_Robonomics functions and class for working with substrate
    """

    def __init__(self):
        logger.info(ROBONOMICS_ACCOUNT.get_address())

    def post(self, ipfs_hash: tp.Optional[str] = None) -> DataLogResponse:
        """This function is used to send data to robonomics

        Args:
            ipfs_hash (string): this hash is from the ipfs

        Returns:
            String: the hash of the data
        """
        try:
            DATALOG_CLIENT = Datalog(account=ROBONOMICS_ACCOUNT,
                                     wait_for_inclusion=False, )
            logger.info("Posting data '{ipfs_hash}' to Robonomics datalog")
            txn_hash: str = DATALOG_CLIENT.record(ipfs_hash)
            datalog_index: int = DATALOG_CLIENT.get_index(ROBONOMICS_ACCOUNT.get_address())['end'] - 1

            logger.info(
                f"Data '{ipfs_hash}' has been posted to the Robonomics datalog. {txn_hash=} and datalog index {datalog_index=}")
            return DataLogResponse(txn_hash, datalog_index)
        except Exception as error:
            logger.error("Failed to send datalog: %s", error)
            return False

    def read(self) -> tp.Optional[DatalogTyping]:
        """This is used to read data from robonomics datalog

        Returns:
            Tuple: the hash and the timestamp
        """
        DATALOG_CLIENT = Datalog(account=ROBONOMICS_ACCOUNT,
                                 wait_for_inclusion=False, )

        return DATALOG_CLIENT.get_item(ROBONOMICS_ACCOUNT.get_address())

    def get_by_index(self, datalog_index) -> tp.Optional[DatalogTyping]:
        DATALOG_CLIENT = Datalog(account=ROBONOMICS_ACCOUNT,
                                 wait_for_inclusion=False, )

        return DATALOG_CLIENT.get_item(ROBONOMICS_ACCOUNT.get_address(), datalog_index)
