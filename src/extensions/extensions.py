"""
Extensions module
Each extension is initialized when app is created.
"""

from flask_cors import CORS  # pylint: disable=import-error
from flask_mail import Mail  # pylint: disable=import-error

cors = CORS()
mail = Mail()
