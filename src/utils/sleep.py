import time
import asyncio

async def sleep():
    print(f'Time: {time.time()}')
    await asyncio.sleep(1)