import secrets

def generate_random_secrets():
    numbers = 16
    return secrets.token_hex(numbers)
