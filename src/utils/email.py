from flask_mail import Message  # pylint: disable=import-error
from src.extensions.extensions import mail  # pylint: disable=import-error
from src.config.config import DevelopmentConfig  # pylint: disable=import-error

def send_mail(recipient_email, template):
    msg = Message(
        subject="Brand Verification ID",
        recipients=[recipient_email],
        html=template,
        sender=DevelopmentConfig.MAIL_DEFAULT_SENDER
    )
    mail.send(msg)
    return "Message sent!"
