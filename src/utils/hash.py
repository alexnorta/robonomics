import base64
from Crypto.Cipher import AES  # pylint: disable=import-error
from Crypto.Util.Padding import pad, unpad  # pylint: disable=import-error

from src.config.config import DevelopmentConfig  # pylint: disable=import-error


class AESCryptor():
    def __init__(self):
        '''
        Build a AES object
        key: Secret key, byte data
        mode: There are only two usage modes, AES.MODE_CBC, AES.MODE_ECB
        iv:  iv Offset, byte data
        paddingMode: Fill mode, default to NoPadding, Optional NoPadding，ZeroPadding，PKCS5Padding，PKCS7Padding
        characterSet: Character set encoding
        '''
        self.key = DevelopmentConfig.SECRET_KEY
        self.iv = DevelopmentConfig.SECRET_IV.encode('utf-8')
        print("key", self.key)
        print("IV", self.iv)

    def encrypt(self, data):
        data = pad(data.encode(), 16)
        cipher = AES.new(self.key.encode('utf-8'), AES.MODE_CBC, self.iv)
        return base64.b64encode(cipher.encrypt(data))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key.encode('utf-8'), AES.MODE_CBC, self.iv)
        return unpad(cipher.decrypt(enc), 16)
