from flask_sqlalchemy import SQLAlchemy  # pylint: disable=import-error
from sqlalchemy.exc import IntegrityError, SQLAlchemyError  # pylint: disable=import-error
from flask import Blueprint, request, jsonify, abort, render_template

from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_409_CONFLICT, HTTP_404_NOT_FOUND,  HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR  # pylint: disable=import-error
from src.models.brand_model import Brand  # pylint: disable=import-error
from src.utils.logger import get_logger  # pylint: disable=import-error
from src.utils.email import send_mail  # pylint: disable=import-error
from src.utils.token import generate_random_secrets  # pylint: disable=import-error

db = SQLAlchemy()

brand_record = Blueprint("brand", __name__, url_prefix="/api/v1/brand")
logger = get_logger(__name__)


@brand_record.post('/new')
def register_a_brand() -> Brand:
    """
    Create a new brand.

    :type user: Brand

    :return: Optional[Brand]
    """
    brand = Brand.query.filter_by(host_address=request.json['host_address']).first()
    try:
        if request.method == 'POST':
            if not request.json['brand_name'] or not request.json['public_key']:
                return jsonify({
                    'error': 'Please enter all the fields'
                }), HTTP_400_BAD_REQUEST
            elif brand:
                return jsonify({
                    'error': f" {request.json['host_address']} already exist"
                }), HTTP_409_CONFLICT
            else:
                logger.info(request.json)
                brand_name = request.json['brand_name']
                public_key = request.json['public_key']
                host_address = request.json['host_address']
                host_email = request.json['host_email']
                status = request.json['status']
                new_brand = Brand(brand_name=brand_name, public_key=public_key,
                                  host_address=host_address, host_email=host_email, status=status)
                db.session.add(new_brand)
                db.session.commit()
                logger.info(new_brand)
                return jsonify({
                    'brand_id': new_brand.id,
                    'brand_name': new_brand.brand_name,
                    'public_key': new_brand.public_key,
                    'host_address': new_brand.host_address,
                    'host_email': new_brand.host_email,
                    'status': new_brand.status,
                    'created_at': new_brand.created_at,
                    'updated_at': new_brand.updated_at,
                }), HTTP_201_CREATED
    except IntegrityError as error:
        db.session.rollback()
        logger.error(error.orig)
        return jsonify({"error": error.orig}), HTTP_500_INTERNAL_SERVER_ERROR
    except SQLAlchemyError as error:
        logger.error(
            f"Unexpected error when creating brand: {error.orig} for {error.params}")
        return jsonify({"error": error}), HTTP_500_INTERNAL_SERVER_ERROR

@brand_record.get('/all')
def get_all_brands():
    found_all_brand = Brand.query.all()
    print("all brand", found_all_brand)
    data = []
    for brand in found_all_brand:
        data.append({
            'brand_id': brand.id,
            'brand_name': brand.brand_name,
            'public_key': brand.public_key,
            'host_address': brand.host_address,
            'host_email': brand.host_email,
            'status': brand.status,
            'created_at': brand.created_at,
            'updated_at': brand.updated_at,
        })
    return jsonify({"data": data}), HTTP_200_OK


@brand_record.get('/<int:id>')
def get_single_brand(id):
    brand = Brand.query.filter_by(id=id).first()
    if brand:
        return jsonify({
            'brand_id': brand.id,
            'brand_name': brand.brand_name,
            'public_key': brand.public_key,
            'host_address': brand.host_address,
            'host_email': brand.host_email,
            'status': brand.status,
            'created_at': brand.created_at,
            'updated_at': brand.updated_at,
        }), HTTP_200_OK,
    return jsonify({"message": f"Brand with {id} does not exist"}), HTTP_404_NOT_FOUND


@brand_record.put('/<int:id>')
def update_brand(id):
    brand = Brand.query.filter_by(id=id).first()
    if request.method == 'PUT':
        if brand:
            brand.brand_name = request.json['brand_name']
            brand.public_key = request.json['public_key']
            brand.host_address = request.json['host_address']
            brand.host_email = request.json['host_email']

            db.session.commit()
            return jsonify({
                'brand_id': brand.id,
                'brand_name': brand.brand_name,
                'public_key': brand.public_key,
                'host_address': brand.host_address,
                'host_email': brand.host_email,
                'status': brand.status,
                'created_at': brand.created_at,
                'updated_at': brand.updated_at,
            }), HTTP_200_OK

    return jsonify({"message": f"Brand with {id} does not exist"}), HTTP_404_NOT_FOUND


@brand_record.delete('/<int:id>')
def delete(id):
    # brand = Brand.query.filter_by(id=id).first()
    # if brand:
            db.session.query(Brand).filter(Brand.id==id).delete()
            db.session.commit()
            # db.session.delete(brand)
            # db.session.commit()
            return jsonify({"data": True}), HTTP_200_OK
    # return jsonify({"message": f"Brand with {id} does not exist"}), HTTP_404_NOT_FOUND


@brand_record.get('/send_email')
def send_email_on_verification():
    host_email = request.args['host_email']
    brand_id = request.args['brand_id']
    found_brand = Brand.query.filter_by(id=brand_id).first()
    if found_brand.host_email == host_email:
        token = generate_random_secrets()
        template = render_template('brand_activation.html', token_id=token)
        send_mail(host_email, template)
        Brand.update_brand_with_secret_id(brand_id, token)
        return jsonify({"data": "Email sent to {host_email}"}), HTTP_200_OK

    else:
        return jsonify({"data": "Email could be sent to {host_email}"})


@brand_record.get('/verify_secret')
def verify_secret():
    secrets_id = request.args['secrets_id']
    found_secret = Brand.query.filter_by(secrets_id=secrets_id).first()
    if found_secret.secrets_id == secrets_id:
        found_secret.secrets_id = ''
        found_secret.status = True
        db.session.merge(found_secret)
        db.session.commit()
        return jsonify({"data": True}), HTTP_200_OK

    else:
        return jsonify({"data": False})
