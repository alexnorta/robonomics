import json
from flask import Blueprint, request, jsonify, abort

from src.constants.http_status_codes import HTTP_200_OK, HTTP_403_FORBIDDEN, HTTP_400_BAD_REQUEST, HTTP_503_SERVICE_UNAVAILABLE  # pylint: disable=import-error
from src.services.ipfs import IpfsHttpClient  # pylint: disable=import-error
from src.utils.logger import get_logger  # pylint: disable=import-error
from src.services.polkadot import verify_signature  # pylint: disable=import-error
from src.utils.hash import AESCryptor  # pylint: disable=import-error
from src.services.robonomics import RobonomicsClient  # pylint: disable=import-error
from src.models.transaction_model import Transaction  # pylint: disable=import-error
from src.models.brand_model import Brand  # pylint: disable=import-error

logger = get_logger("logisticBDT IPFS Route")

ipfs = Blueprint("ipfs", __name__, url_prefix="/api/v1/ipfs")
logger = get_logger(__name__)

robonomics_client = RobonomicsClient()
ipfs_client = IpfsHttpClient()

aes = AESCryptor()


@ipfs.post('/save-item')
def create_ipfs_data():
    """ This is used to save item to ipfs and Robonomics

    Returns:
        _type_: return the hash saved on Robonomics
    """
    logger.info(request.json)
    received_raw_data = {
        'message': request.json['message'],
        'newPairAddress': request.json['newPairAddress'],
        'signature': request.json['hexSignature']
    }

    logger.info(received_raw_data)
    is_data_valid = verify_signature(
        received_raw_data['message'], received_raw_data['signature'], received_raw_data['newPairAddress'])
    if is_data_valid:
        decrypted = aes.decrypt(received_raw_data['message'])
        result = json.loads(decrypted.decode("utf-8", "ignore"))
        json_result = json.loads(result)
        print("json_result", json_result)
        brand = Brand.get_brand_by_public_key(json_result["public_key"])
        print("brand", brand)
        if brand == 'No brand found':
            return jsonify({"error": "Cannot add an items because brand is not registered or verified"}), HTTP_403_FORBIDDEN
        else:
            added_to_ipfs = ipfs_client.upload_json_metadata(json_result, brand, received_raw_data['message'])
            print("add to ipfs", added_to_ipfs)
            datalog_response = robonomics_client.post(added_to_ipfs["cid"])
            print(f"CID posted to robonomics: {datalog_response.trx_hash}")
            transaction_results = Transaction.register_a_transaction(json_result, added_to_ipfs,
                                                                     datalog_response.trx_hash, brand,
                                                                     datalog_response.index)
            print("Transaction created Successfully", transaction_results)
            found_transactions = {
                "transaction_id": transaction_results.id,
                "pid": transaction_results.pid,
                "cid": transaction_results.cid,
                "pk1": transaction_results.pk1,
                "pk2": transaction_results.pk2,
                "robonomics_hash": transaction_results.rb_index,
                "created_at": transaction_results.created_at,
                "updated_at": transaction_results.updated_at,
                "datalog_index:": transaction_results.datalog_index
            }
            print("transaction_results", transaction_results)
            return jsonify({"data": added_to_ipfs, "Robonomics data hash": datalog_response.trx_hash,
                            "transaction": found_transactions}), HTTP_200_OK
            
    else:
        return jsonify({"error": "signature is not valid"})

@ipfs.get('/get_ipfs/<string:cid>')
def get_ipfs(cid):
    item_cid = cid
    metadata_from_ipfs = ipfs_client.get_single_item(
        item_cid)  # pylint: disable=undefined-variable

    return jsonify({
        'data': metadata_from_ipfs
    }), HTTP_200_OK


@ipfs.post('/add_image_to_ipfs')
def upload_image():
    print("request", request.files)
    try:
        if "inputFile" in request.files:
            file = request.files["inputFile"]
            logger.info("file name: {}".format(
                file.filename), {'app': 'dfile-up-req'})
            res = ipfs_client.add_bytes(file)
            logger.info("upload res: {}".format(res), {'app': 'dfile-up-res'})
            return jsonify({
                'cid': res
            }), HTTP_200_OK

        abort(HTTP_400_BAD_REQUEST)

    except Exception as e:
        logger.exception("Upload Error! exception:{}".format(str(e)))
        return "Upload Error! \n", HTTP_503_SERVICE_UNAVAILABLE
