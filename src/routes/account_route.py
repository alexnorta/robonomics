from flask_sqlalchemy import SQLAlchemy  # pylint: disable=import-error
from sqlalchemy.exc import IntegrityError, SQLAlchemyError  # pylint: disable=import-error
from flask import Blueprint, request, jsonify

from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_409_CONFLICT, HTTP_404_NOT_FOUND,  HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR,HTTP_401_UNAUTHORIZED  # pylint: disable=import-error
from src.models.account_model import Account  # pylint: disable=import-error
from src.utils.logger import get_logger  # pylint: disable=import-error

db = SQLAlchemy()

account_record = Blueprint("account", __name__, url_prefix="/api/v1/account")
logger = get_logger(__name__)

@account_record.post('/new')
def register_an_account() -> Account:
    """
    Save a user keystone.

    :type user: Account

    :return: Optional[Account]
    """
    account = Account.query.filter_by(wallet_name=request.json['wallet_name']).first()
    try:
        if request.method == 'POST':
            if not request.json['wallet_name'] or not request.json['keystone_json_file']:
                return jsonify({
                    'error': 'Please enter wallet name or keystone_json_file'
                }), HTTP_400_BAD_REQUEST
            elif account:
                return jsonify({
                    'error': f" {request.json['wallet_name']} already exist"
                }), HTTP_409_CONFLICT
            else:
                logger.info(request.json)
                wallet_name = request.json['wallet_name']
                keystone_json_file = request.json['keystone_json_file']
                password = request.json['password']
                address = request.json['address']

                new_account = Account(wallet_name=wallet_name, keystone_json_file=keystone_json_file, password=password, address=address)
                db.session.add(new_account)
                db.session.commit()
                logger.info(new_account)
                return jsonify({
                    'account_id': new_account.id,
                    'wallet_name': new_account.wallet_name,
                    'address': new_account.address,
                    'keystone_json_file': new_account.keystone_json_file,
                    'created_at': new_account.created_at,
                }), HTTP_201_CREATED
    except IntegrityError as error:
        db.session.rollback()
        logger.error(error.orig)
        return jsonify({"error": error.orig}), HTTP_500_INTERNAL_SERVER_ERROR
    except SQLAlchemyError as error:
        logger.error(
            f"Unexpected error when creating account: {error.orig} for {error.params}")
        return jsonify({"error": error}), HTTP_500_INTERNAL_SERVER_ERROR

@account_record.get('/<string:wallet_name>')
def get_an_account_by_wallet_name(wallet_name):
    account = Account.query.filter_by(wallet_name=wallet_name).first()
    if account:
        return jsonify({
            'account_id': account.id,
            'wallet_name': account.wallet_name,
            'keystone_json_file': account.keystone_json_file,
            'created_at': account.created_at,
        }), HTTP_200_OK,
    return jsonify({"message": f"Account with {wallet_name} does not exist"}), HTTP_404_NOT_FOUND

@account_record.post('/confirm_ownership')
def confirm_ownership():
    password = request.json['password']
    address = request.json['address']  
    print("address: ", address)  
    account = Account.query.filter_by(address=address).first()
    if account and account.verify_password(password):
        return jsonify({
            "Success": True
        }), HTTP_200_OK,
    return jsonify({"Success": False}),HTTP_401_UNAUTHORIZED 

@account_record.get('/verify/<string:address>')
def verify_ownership(address):
    account = Account.query.filter_by(address=address).first()
    if account:
        return jsonify({
            "Success": True
        }), HTTP_200_OK,
    return jsonify({"Success": False}),HTTP_200_OK 
