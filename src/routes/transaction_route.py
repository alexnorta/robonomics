import json
from sqlalchemy import desc  # pylint: disable=import-error

from flask_sqlalchemy import SQLAlchemy  # pylint: disable=import-error
from flask import Blueprint, request, jsonify
from src.models.transaction_model import Transaction  # pylint: disable=import-error
from src.services.robonomics import RobonomicsClient  # pylint: disable=import-error
from src.services.ipfs import IpfsHttpClient  # pylint: disable=import-error

from src.constants.http_status_codes import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND  # pylint: disable=import-error
from src.utils.logger import get_logger  # pylint: disable=import-error
from src.services.polkadot import verify_signature  # pylint: disable=import-error
from src.utils.hash import AESCryptor  # pylint: disable=import-error
from src.models.brand_model import Brand  # pylint: disable=import-error
from src.config.config import DevelopmentConfig  # pylint: disable=import-error

db = SQLAlchemy()
ipfs_client = IpfsHttpClient()
robonomics_client = RobonomicsClient()
aes = AESCryptor()

transaction_record = Blueprint(
    "transaction", __name__, url_prefix="/api/v1/transaction")
logger = get_logger(__name__)


@transaction_record.get('/all')
def get_all_transaction():
    found_all_transactions = Transaction.query.all()
    data = []
    for transaction in found_all_transactions:
        data.append({
            'transaction_id': transaction.id,
            'brand_public_key': transaction.brand_public_key,
            'pid': transaction.pid,
            'cid': transaction.cid,
            'pk1': transaction.pk1,
            'pk2': transaction.pk2,
            'rb_index': transaction.rb_index,
            'last_transaction': transaction.last_transaction,
            'created_at': transaction.created_at,
            'updated_at': transaction.updated_at,
            'datalog_index': transaction.datalog_index
        })
    return jsonify({"data": data}), HTTP_200_OK


@transaction_record.get('/pid_exit')
def get_and_check_pid_exist():
    pid = request.args['pid']
    found_pids = Transaction.query.filter_by(pid=pid).all()
    ipfs_results = []
    if found_pids:
        for pid in found_pids:
            cid = pid.cid
            url = "https://" + DevelopmentConfig.IPFS_CONNECT_URL + '/ipfs/' + cid
            ipfs = ipfs_client.get_single_item(cid)
            ipfs_results.append({
                'attributes': ipfs["attributes"],
                'description': ipfs["description"],
                'product_image': ipfs["product_image"],
                'product_name': ipfs["product_name"],
                'rb_hash': pid.rb_index,
                'datalog_index': pid.datalog_index,
                'created_at': ipfs["timestamp"],
                'ipfs_cid': url,
            })
        return jsonify({"data": ipfs_results}), HTTP_200_OK

    else:
        return jsonify({"data": False})


@transaction_record.get('/pk2')
def get_all_items_by_owner():
    pk2 = request.args['pk2']
    found_pk2 = Transaction.query.filter_by(pk2=pk2, last_transaction = True).all()
    ipfs_results = []
    if found_pk2:
        for pid in found_pk2:
            cid = pid.cid
            url = "https://" + DevelopmentConfig.IPFS_CONNECT_URL + '/ipfs/' + cid
            ipfs = ipfs_client.get_single_item(cid)
            ipfs_results.append({
                'attributes': ipfs["attributes"],
                'description': ipfs["description"],
                'product_image': ipfs["product_image"],
                'product_name': ipfs["product_name"],
                'rb_hash': pid.rb_index,
                'datalog_index': pid.datalog_index,
                'created_at': ipfs["timestamp"],
                'ipfs_cid': url,
            })
        return jsonify({"data": ipfs_results}), HTTP_200_OK

    else:
        return jsonify({"data": 'The owner does not have any items'}), HTTP_404_NOT_FOUND


@transaction_record.get('/pid')
def get_transaction_by_pid():
    pid = request.args['pid']
    found_pids = Transaction.query.filter_by(pid=pid).all()
    print("found transactions", found_pids)
    if found_pids:
        data = []
        for pid in found_pids:
            data.append({
                'transaction_id': pid.id,
                'brand_public_key': pid.brand_public_key,
                'pid': pid.pid,
                'cid': pid.cid,
                'pk1': pid.pk1,
                'pk2': pid.pk2,
                'rb_index': pid.rb_index,
                'last_transaction': pid.last_transaction,
                'created_at': pid.created_at,
                'updated_at': pid.updated_at,
                'datalog_index': pid.datalog_index
            })
        return jsonify({"data": data}), HTTP_200_OK

    else:
        return jsonify({"data": "No transaction found for this pid"}), HTTP_404_NOT_FOUND


@transaction_record.get('/get_datalog')
def get_robonomics_datalog_by_index():
    trx_hash = request.args['trx-hash']
    print("get_datalog: %s" % trx_hash)
    found_transactions = Transaction.query.filter_by(rb_index=trx_hash).all()
    print('found_transactions', found_transactions)
    data = []
    if found_transactions:
        for transaction in found_transactions:
            datalog = robonomics_client.get_by_index(transaction.datalog_index)
            data.append(datalog)

        return jsonify({"data": datalog}), HTTP_200_OK

    else:
        return jsonify({"data": False})

@transaction_record.post('/transfer_nft')
def transfer_nft():
    logger.info(request.json)
    received_raw_data = {
        'message': request.json['message'],
        'newPairAddress': request.json['newPairAddress'],
        'signature': request.json['hexSignature']
    }
    logger.info(received_raw_data)
    new_pk = request.json['public_key']
    is_data_first_valid = verify_signature(
        received_raw_data['message'], received_raw_data['signature'], received_raw_data['newPairAddress'])
    if is_data_first_valid:
        first_decrypted = aes.decrypt(received_raw_data['message'])
        first_result = json.loads(first_decrypted.decode("utf-8", "ignore"))
        first_json_result = json.loads(first_result)
        print("first_json_result", first_json_result)
    is_data_second_valid = verify_signature(
        first_json_result['message'], first_json_result['hexSignature'], first_json_result['newPairAddress'])
    if is_data_second_valid:
        second_decrypted = aes.decrypt(first_json_result['message'])
        second_result = json.loads(second_decrypted.decode("utf-8", "ignore"))
        second_json_result = json.loads(second_result)
        print("second_json_result", second_json_result)
        if second_json_result:
            found_pid = Transaction.query.filter_by(
                pid=second_json_result["pid"], last_transaction = True).order_by(desc('created_at')).limit(1).first()
            print("found_pid", found_pid)              
            added_to_ipfs = ipfs_client.upload_json_metadata_on_transfer(
                second_json_result, found_pid, new_pk,first_json_result['message'])
            print("add to ipfs", added_to_ipfs)
            datalog_response = robonomics_client.post(added_to_ipfs["cid"])
            print ('datalog_response', datalog_response)
            print(f"CID posted to robonomics: {datalog_response.trx_hash}")
            transaction_results = Transaction.transfer_ownership(second_json_result, added_to_ipfs,
                                                                datalog_response.trx_hash, found_pid, new_pk,
                                                                datalog_response.index)
            Transaction.update_an_item_by_owner_last_transaction(found_pid.pk2, found_pid.pid)
            print("Transaction created Successfully", transaction_results)
            found_transactions = {
                "transaction_id": transaction_results.id,
                "pid": transaction_results.pid,
                "cid": transaction_results.cid,
                "pk1": transaction_results.pk1,
                "pk2": transaction_results.pk2,
                "robonomics_hash": transaction_results.rb_index,
                'last_transaction': transaction_results.last_transaction,
                "created_at": transaction_results.created_at,
                "updated_at": transaction_results.updated_at,
                "datalog_index:": transaction_results.datalog_index
            }
            print("transaction_results", transaction_results)

            return jsonify({"data": added_to_ipfs, "Robonomics data hash": datalog_response.trx_hash,
                            "transaction": found_transactions}), HTTP_200_OK
        else:
            return jsonify({"data": "Key signature verification not succeeded"}), HTTP_400_BAD_REQUEST
    else:
        return jsonify({"data": False})
